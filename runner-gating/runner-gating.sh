#!/bin/sh

# exit when any command fails
set -e

GITLAB="https://gitlab.freedesktop.org"
GITLAB_API="${GITLAB}/api/v4"
GITLAB_CI_OK_GROUP=102611

GITLAB_STAGE=$1
GITLAB_STAGE=${GITLAB_STAGE:=pre_build_script}

alias curl="/host/bin/curl -s -L --retry 4 -f --retry-delay 60 --cacert /host/ca-certificates.crt"
alias jq="/host/bin/jq"

exit_0() {
  echo $1

  if [ x"$GITLAB_STAGE" = x"pre_get_sources_script" ] && [ x"$CI_PRE_CLONE_SCRIPT" != x"" ]
  then
    echo "Running pre-clone script: '$CI_PRE_CLONE_SCRIPT'"
    eval "$CI_PRE_CLONE_SCRIPT"
  fi
  exit 0
}

greet_and_exit()
{
  exit_0 "Thank you for contributing to freedesktop.org"
}

# retrieving the job data is reliable, because CI_JOB_TOKEN is only
# valid during the execution of the job and can not be tempered with
# Note: we use an actual file because we are in sh, and new lines gets
# interpreted by `echo`
curl --header "Authorization: Bearer ${CI_JOB_TOKEN}" -o job.json ${GITLAB_API}/job

# extract some reliable information
GATING_USER_ID=$(cat job.json | jq -r '.user.id')
GATING_USER=$(cat job.json | jq -r '.user.username')
GATING_PROJECT_ID=$(cat job.json | jq -r '.pipeline.project_id')
GATING_SOURCE=$(cat job.json | jq -r '.pipeline.source')
GATING_COMMIT=$(cat job.json | jq -r '.commit.id')

if [ -z "$GATING_USER" ]
then
  echo "Something wrong happened while fetching the user of the job"
  cat job.json
  rm job.json
  echo ""
  # Something wrong happened, we can not trust the user bail out
  exit 1
fi

rm job.json

echo "Checking if the user of the pipeline is allowed..."

# members of the CI-OK group are allowed to run all sort of pipelines
if curl --header @$CI_GATING_TOKEN \
        ${GITLAB_API}/groups/${GITLAB_CI_OK_GROUP}/members/all\?user_ids\=${GATING_USER_ID} | \
          jq -e '.[].id' > /dev/null
then
  greet_and_exit
fi

# user is not a CI-OK group member, more checks are required
#
# Everything from now will fail for private project. We need to make sure
# submitters of valid private projects are validated above.

# If the pipeline runs in a public project of group namespace, this is fine
# This happens also with Merge Request Pipelines, when they are not tagged
# as 'fork' (if the user has merge or push permissions to the target branch
# but is not part of the CI-OK group, i.e. has been granted individual push
# permissions to the project itself).
GATING_GROUP=$(curl ${GITLAB_API}/projects/${GATING_PROJECT_ID} | jq -r '.namespace.kind' || echo "private")

echo "Checking if the job's project is part of a well-known group..."

if [ x"$GATING_GROUP" = x"group" ]
then
  greet_and_exit
fi

# Check if this is a merge request, but the users can not merge or push to
# the target branch.
# The only place where we have the target repo is in CI_MERGE_REQUEST_PROJECT_ID
# To prevent an override, double check that the data is valid
curl -o mr.json ${GITLAB_API}/projects/${CI_MERGE_REQUEST_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID} || echo "{}" > mr.json

GATING_MR_COMMIT=$(cat mr.json | jq -r '.sha')
GATING_MR_SOURCE_PROJECT_ID=$(cat mr.json | jq -r '.source_project_id')
rm mr.json

echo "Checking if the job is part of an official MR pipeline..."

# We check that the merge request exists, and that it matches
# the information we trust
if [ x"$GATING_SOURCE" = x"merge_request_event" ] && \
   [ x"$GATING_MR_COMMIT" = x"$GATING_COMMIT" ] && \
   [ x"$GATING_MR_SOURCE_PROJECT_ID" = x"$GATING_PROJECT_ID" ]
then
  greet_and_exit
fi

echo "***************************************************************************************************************************"
echo ""
echo "Looks like you don't have enough privileges, please see https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/540"
echo ""
echo "***************************************************************************************************************************"

# the following line needs to be commented to actually enforce the check
#echo ""; exit_0 "Skipping permission check for now"

exit 1
