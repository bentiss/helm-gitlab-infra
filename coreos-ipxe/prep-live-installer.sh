#!/bin/bash

METADATA_URL="https://metadata.platformequinix.com/metadata"
USERDATA_URL="https://metadata.platformequinix.com/userdata"


############################################################
# download butane to be able to generate ignition files
############################################################


curl -L -o /usr/local/bin/butane https://github.com/coreos/butane/releases/download/v0.18.0/butane-$(arch)-unknown-linux-gnu
chmod +x /usr/local/bin/butane


############################################################
# download gomplate to be able to generate butane files
############################################################

GOMPLATE_ARCH=$(arch)
if [ "$GOMPLATE_ARCH" = "x86_64" ]
then
  GOMPLATE_ARCH=amd64
fi
curl -L -o /usr/local/bin/gomplate https://github.com/hairyhenderson/gomplate/releases/download/v3.11.5/gomplate_linux-$GOMPLATE_ARCH
chmod +x /usr/local/bin/gomplate


############################################################
# clone the fdo-infra repo to have the files available
############################################################


FDO_INFRA_REPO=$(curl ${USERDATA_URL} | grep -e '^set FDO_INFRA_URL' | cut -d ' ' -f 3).git

FDO_INFRA_REPO=${FDO_INFRA_REPO:-https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra.git}

FDO_INFRA_DIR="/root/fdo_infra/"

git clone ${FDO_INFRA_REPO} ${FDO_INFRA_DIR}

cd ${FDO_INFRA_DIR}


############################################################
# Generate the installer ignition file depending of the usage
# (k3s, runner) and plan of the target (c3.large.arm64, etc..)
############################################################


# retrieve the plan and tags associated with the machine
METADATA=$(curl $METADATA_URL)
PLAN=$(echo ${METADATA} | jq -r '.plan')
IS_CI=$(echo ${METADATA} | jq -r 'any(.tags[] == "ci"; .)')
IS_K3S=$(echo ${METADATA} | jq -r 'any(.tags[] == "k3s"; .)')

COREOS_INSTALLER_EXTRA_CONFIG=./coreos-ipxe/${PLAN}.yaml

if [[ ! -f ${COREOS_INSTALLER_EXTRA_CONFIG} ]]
then
  echo File ${COREOS_INSTALLER_EXTRA_CONFIG} is not found on the repo ${FDO_INFRA_REPO}, aborting...
  exit 1
fi

# append the plan config to the coreos-installer config directory
cp ${COREOS_INSTALLER_EXTRA_CONFIG} /etc/coreos/installer.d/50-extra.yaml

# find out the correct butane config file
if [[ "$IS_CI" == "true" ]]
then
  BUTANE_DIR=gitlab-runner-provision
elif [[ "$IS_K3S" == "true" ]]
then
  BUTANE_DIR=gitlab-k3s-provision
else
  echo "no 'ci' or 'k3s' tags attached to the machine, giving up"
  exit 1
fi

BUTANE_FILE=${BUTANE_DIR}/${PLAN}.butane

if [[ ! -f ${BUTANE_FILE} ]]
then
  echo File ${BUTANE_FILE} is not found on the repo ${FDO_INFRA_REPO}, aborting...
  exit 1
fi

# generate the butane config(s)
gomplate --file ${BUTANE_DIR}/${BUTANE_DIR/-provision}.butane \
  --datasource config=${BUTANE_DIR}/config.yaml \
  --datasource metadata=${METADATA_URL} | \
  butane --files-dir ${BUTANE_DIR} --strict --pretty \
  > ${BUTANE_DIR}/${BUTANE_DIR/-provision}.ign
gomplate --file $BUTANE_FILE \
  --datasource config=${BUTANE_DIR}/config.yaml \
  --datasource metadata=${METADATA_URL} | \
  butane --files-dir . --strict --pretty > /root/config.ign


############################################################
# Generate the network config
# (ideally this should be done by afterburn, but it's not
# in place for the packet provider)
############################################################

# generate stable NIC names and bond slaves

for interface in $(echo $METADATA | jq -c '.network.interfaces[]')
do
  MAC=$(echo $interface| jq -r '.mac')
  IFNAME=$(ip --json a | jq -r ".[] | select(.address==\"$MAC\")| .ifname")

  cat <<EOF > /etc/NetworkManager/system-connections/bond0-slave-${IFNAME}.nmconnection
[connection]
id=bond0-slave-${IFNAME}
type=ethernet
interface-name=${IFNAME}
master=bond0
slave-type=bond
EOF
  chmod 0600 /etc/NetworkManager/system-connections/bond0-slave-${IFNAME}.nmconnection

done

# bond public
PUBLIC_IPv4=$(echo $METADATA | jq -cr '.network.addresses[] | select(.address_family==4 and .public==true)')
PUBLIC_IPv6=$(echo $METADATA | jq -cr '.network.addresses[] | select(.address_family==6 and .public==true)')
PRIVATE_IPv4=$(echo $METADATA | jq -cr '.network.addresses[] | select(.address_family==4 and .public==false)')

cat <<EOF > /etc/NetworkManager/system-connections/bond0.nmconnection
[connection]
id=bond0
type=bond
interface-name=bond0

[bond]
miimon=100
mode=$(echo $METADATA | jq -cr '.network.bonding.mode')
downdelay=200
updelay=200
xmit_hash_policy=layer3+4

[ipv4]
address1=$(echo $PUBLIC_IPv4 | jq -r '.address')/$(echo $PUBLIC_IPv4 | jq -r '.cidr'),$(echo $PUBLIC_IPv4 | jq -r '.gateway')
address2=$(echo $PRIVATE_IPv4 | jq -r '.address')/$(echo $PRIVATE_IPv4 | jq -r '.cidr')
dhcp-hostname=$(hostname)
dns=147.75.207.207;147.75.207.208
dns-search=
may-fail=false
route1=10.0.0.0/8,$(echo $PRIVATE_IPv4 | jq -r '.gateway')
method=manual

[ipv6]
address1=$(echo $PUBLIC_IPv6 | jq -r '.address')/$(echo $PUBLIC_IPv6 | jq -r '.cidr'),$(echo $PUBLIC_IPv6 | jq -r '.gateway')
dns-search=
may-fail=true
method=manual
EOF
chmod 0600 /etc/NetworkManager/system-connections/bond0.nmconnection

for vlan in $(echo $METADATA | jq -c '.customdata.vlans[]')
do
  VLAN_ID=$(echo $vlan | jq -r '.vlan')
  IP=$(echo $vlan | jq -r '.ip')
  IPv6=$(echo $vlan | jq -r '.ipv6')

  cat <<EOF > /etc/NetworkManager/system-connections/bond0.${VLAN_ID}.nmconnection
[connection]
id=bond0.${VLAN_ID}
type=vlan
interface-name=bond0.${VLAN_ID}
zone=trusted

[vlan]
egress-priority-map=
flags=1
id=${VLAN_ID}
ingress-priority-map=
parent=bond0

[ipv4]
address1=${IP}
dns-search=
may-fail=false
method=manual

[ipv6]
addr-gen-mode=default
address1=${IPv6}
method=auto

EOF
  chmod 0600 /etc/NetworkManager/system-connections/bond0.${VLAN_ID}.nmconnection

done
