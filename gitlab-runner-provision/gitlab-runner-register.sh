#!/bin/bash

# first, retrieve the customdata for the server
for s in $(curl https://metadata.packet.net/metadata \
           | jq -r '.customdata.environment | to_entries | map( "\(.key)=\(.value | tostring)") | .[]')
do
  export $s
done

if [[ ! -f /etc/gitlab-runner/config.toml ]]
then
  echo "concurrent = ${GITLAB_RUNNER_CONCURRENT}" > /etc/gitlab-runner/config.toml
fi

# ensure we have the proper arch jq and curl
ln -sf ./jq-$(arch) /var/host/bin/jq
ln -sf ./curl-$(arch) /var/host/bin/curl

GATING_SCRIPT="https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/raw/main/runner-gating/runner-gating.sh"
CURL_CMD="/host/bin/curl -s -L --cacert /host/ca-certificates.crt --retry 4 -f --retry-delay 60"

podman exec -ti gitlab-runner gitlab-runner register \
    --name $(hostname) \
    --non-interactive \
    --limit $GITLAB_RUNNER_CONCURRENT \
    --request-concurrency 2 \
    --executor docker \
    --docker-host unix:///run/podman/podman.sock \
    --docker-pull-policy if-not-present \
    --docker-image alpine:latest \
    --docker-privileged \
    --docker-devices /dev/kvm \
    --docker-volumes "/var/cache/gitlab-runner/cache:/cache" \
    --docker-volumes "/var/host:/host:ro" \
    --custom_build_dir-enabled=true \
    --registration-token $GITLAB_RUNNER_REG_TOKEN \
    --env "DOCKER_TLS_CERTDIR=" \
    --env "FDO_CI_CONCURRENT=$GITLAB_RUNNER_CONCURRENT" \
    --tag-list $GITLAB_RUNNER_TAGS \
    $GITLAB_RUNNER_UNTAGGED \
    --docker-tmpfs /tmp:rw,nosuid,nodev,exec,mode=1777 \
    --docker-extra-hosts "ssh.tmate.io:192.0.2.1" \
    --url https://gitlab.freedesktop.org \
    --pre-get-sources-script "$CURL_CMD $GATING_SCRIPT | sh -s -- pre_get_sources_script" \
    --pre-build-script "$CURL_CMD $GATING_SCRIPT | sh"
