#!/bin/bash

COMMAND="$1"
shift
REPOSITORIES="$@"

function Help () {
  echo usage:
  echo $0 COMMAND ROOT_REPOSITORY [ROOT_REPOSITORY2 [ROOT_REPOSITORY3 ...]]
  echo
  echo where COMMAND is
  echo "  import - import the given ROOT_REPOSITORY in the db"
  echo "  ls - list all of the repositories under the given ROOT_REPOSITORY in the old non-db registry"
  echo
  echo "Note that GRAPHQL_TOKEN environment variable must be set to an admin read_api token"
}

if [ -e "$REPOSITORIES" ]
then
  Help
  exit 1
fi

if [ -e $COMMAND ]
then
  Help
  exit 1
fi

if [ -e $GRAPHQL_TOKEN ]
then
  Help
  exit 1
fi

REGISTRY_POD=$(kubectl -n gitlab get pod -o name -l app=registry | head -n1)

function run_in_registry_pod () {
  local command="$@"
  kubectl -n gitlab exec -ti $REGISTRY_POD -c registry -- $command
}

function import_blob () {
  local tempdir="$1"
  local blob="$2"

  grep -q $blob $tempdir/known_blobs.txt && echo $blob is known && return

  gsutil cp gs://fdo-gitlab-registry/docker/registry/v2/blobs/sha256/${blob:0:2}/${blob}/data \
     gs://fdo-gitlab-registry/gitlab/docker/registry/v2/blobs/sha256/${blob:0:2}/${blob}/data
}

function call_pre_import () {
  local repository="$1"
  local options="$2"

  run_in_registry_pod /bin/registry database import $options --pre-import --repository $repository /etc/docker/registry/config.yml
}

function get_known_blobs () {
  local tempdir="$1"

  >&2 echo "retrieving all known blobs in the registry db"

  gsutil ls gs://fdo-gitlab-registry/gitlab/docker/registry/v2/blobs/sha256/\*\* | cut -d / -f 11 > $tempdir/known_blobs.txt
}

function pre_import () {
  local tempdir="$1"
  local repository="$2"
  local options="$3"

  >&2 echo "pre-import '${repository}' with options: '${options}'"

  BLOBS=$(call_pre_import $repository $options | jq -r 'select(.digest != null) | .digest' | cut -d ':' -f 2 | sort | uniq)

  >&2 echo "copy all blobs from '${repository}' to the new registry"

  gsutil -m cp -r gs://fdo-gitlab-registry/docker/registry/v2/repositories/$repository/_layers/sha256/ \
          gs://fdo-gitlab-registry/gitlab/docker/registry/v2/repositories/$repository/_layers/

  export -f import_blob
  echo $BLOBS | xargs -I {} -d ' ' -n 1 --max-procs=4 bash -c "import_blob $tempdir {}"

  # manually export the list of imported blob without parallelism
  for blob in $BLOBS
  do
    echo $blob >> $tempdir/known_blobs.txt
  done
}

function import () {
  local repository="$1"

  >&2 echo "import '${repository}'"
  run_in_registry_pod /bin/registry database import --repository $repository /etc/docker/registry/config.yml
}

function mark_repo_imported () {
  local repository="$1"

  gsutil -m mv gs://fdo-gitlab-registry/docker/registry/v2/repositories/$repository/_layers/ \
               gs://fdo-gitlab-registry/docker/registry/v2/repositories/$repository/_imported_layers
}

function fetch_gitlab_repositories () {
  local tempdir="$1"
  local namespace="$2"

  >&2 echo "fetching with graphql all repositories under namespace '${namespace}'"

  local data=$(curl --silent "https://gitlab.freedesktop.org/api/graphql" --header "Authorization: Bearer $GRAPHQL_TOKEN" \
                    --header "Content-Type: application/json" --request POST \
                    --data "{\"query\": \"query {namespace (fullPath:\\\"${namespace}\\\") {projects { edges { node {containerRepositories { edges { node { name location }}}}}}}}\"}")

  echo $data | jq -r '.data.namespace.projects.edges[].node |
                        select(.containerRepositories != null) |
                          .containerRepositories.edges[].node.location |
                          sub("registry.freedesktop.org/";"")' > \
    $tempdir/gitlab_${namespace}_registries.txt
}

grep_gitlab_repo () {
  local tempdir="$1"
  local repo="$2"
  local namespace="$3"

  grep -q $repo $tempdir/gitlab_${namespace}_registries.txt && echo $repo
}

function namespace () {
  local repository="$1"

  cut -d/ -f 1 <<< ${repository}
}

function list_repositories () {
  local tempdir="$1"
  local root_repository="$2"
  local namespace=$(namespace $root_repository)

  fetch_gitlab_repositories $tempdir $namespace

  >&2 echo "listing all non-imported repositories for '${root_repository}'"

  for repo in $(gsutil ls -d gs://fdo-gitlab-registry/docker/registry/v2/repositories/${root_repository}/\*\* | \
                  grep /_layers | \
                  awk -F'/_layers' '{print $1}' | \
                  sort | \
                  uniq | \
                  sed 's|gs://fdo-gitlab-registry/docker/registry/v2/repositories/||')
  do
    grep_gitlab_repo $tempdir $repo $namespace
  done
}

function import_repositories () {
  local tempdir="$1"
  local root_repository="$2"

  for repo in $(list_repositories $tempdir $root_repository)
  do
    echo $repo
    # initial pre-import to do the gsutil cp first
    pre_import $tempdir $repo --dry-run
    # actually import the blobs in the db, doing a late gsutil cp for the new ones
    pre_import $tempdir $repo
    import $repo
    mark_repo_imported $repo
  done
}

TMP_DIR=$(mktemp -d)

if [ "$COMMAND" == "import" ]
then
  get_known_blobs $TMP_DIR

  for root_repo in $REPOSITORIES
  do
    import_repositories $TMP_DIR $root_repo
  done
elif [ "$COMMAND" == "ls" ]
then
  for root_repo in $REPOSITORIES
  do
    list_repositories $TMP_DIR $root_repo
  done
fi

rm -rf $TMP_DIR
