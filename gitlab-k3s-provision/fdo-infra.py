#!/usr/bin/env python3
#
# Copyright © 2021 Benjamin Tissoires
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Authors: Benjamin Tissoires <benjamin.tissoires@gmail.com>

import base64
import click
import ipaddress
import json
import logging
import os
import paramiko  # for handling ssh
import packet
import random
import scp
import shlex
import shutil
import subprocess
import sys
import tempfile
import uuid
import yaml

from attrs import define
from pathlib import Path
from typing import List


numeric_level = getattr(logging, "info".upper(), None)
logging.basicConfig(level=numeric_level)


@click.group()
@click.option(
    "--project-id",
    help="the project UUID on packet",
    default=os.getenv("METAL_PROJECT_ID"),
)
@click.option(
    "--project-token",
    help="the project token on packet",
    default=os.getenv("METAL_AUTH_TOKEN"),
)
@click.pass_context
def fdo_infra(ctx, project_id, project_token):
    # ensure that ctx.obj exists and is a dict (in case `fdo_infra()` is called
    # by means other than the `if` block below)
    ctx.ensure_object(dict)
    ctx.obj["project_id"] = project_id
    ctx.obj["project_token"] = project_token


# based on https://github.com/hackersandslackers/paramiko-tutorial/blob/master/paramiko_tutorial/client.py
# MIT Licensed
class RemoteClient(object):
    """Client to interact with a remote host via SSH & SCP."""

    def __init__(
        self,
        host: str,
        user: str,
        port: int = 2222,
    ):
        self.host = host
        self.user = user
        self.port = port
        self._client = self.new_connection()
        self._scp = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.disconnect()

    def new_connection(self):
        """Open connection to remote host."""
        try:
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(
                self.host,
                username=self.user,
                port=self.port,
                timeout=5000,
            )
            return client
        except paramiko.auth_handler.AuthenticationException as e:
            logging.error(
                f"Authentication failed: did you remember to create an SSH key? {e}"
            )
            raise e

    def new_scp(self) -> scp.SCPClient:
        conn = self.new_connection()
        return scp.SCPClient(conn.get_transport())

    @property
    def scp(self):
        if self._scp is None:
            self._scp = self.new_scp()

        return self._scp

    def disconnect(self):
        """Close SSH & SCP connection."""
        self._client.close()
        if self._scp is not None:
            self._scp.close()

    def bulk_upload(self, files: List[str], remote_path: str):
        """
        Upload multiple files to a remote directory.
        :param files: List of local files to be uploaded.
        :type files: List[str]
        """
        try:
            self.scp.put(files, remote_path=remote_path)
            logging.info(
                f"Finished uploading {len(files)} files to {remote_path} on {self.host}"
            )
        except scp.SCPException as e:
            raise e

    def download_file(self, file: str):
        """Download file from remote host."""
        self.scp.get(file)

    def execute_commands(self, commands: List[str]):
        """
        Execute multiple commands in succession.
        :param commands: List of unix commands as strings.
        :type commands: List[str]
        """
        compact = len(commands) > 10
        if compact:
            command = ";".join(commands)
            commands = [
                f"bash -c '{command}'",
            ]
        for cmd in commands:
            stdin, stdout, stderr = self._client.exec_command(cmd)
            stdout.channel.recv_exit_status()
            response = stdout.readlines()
            cmd = f"{cmd[:50]}..." if compact else cmd
            for line in response:
                logging.info(f"INPUT: {cmd} | OUTPUT: {line.rstrip()}")
            errs = stderr.readlines()
            for line in errs:
                logging.warning(f"INPUT: {cmd} | OUTPUT: {line.rstrip()}")

    def execute_command(self, command: str):
        """
        Execute one command.
        :param command: the unix command as a string.
        :type command: str
        """
        stdin, stdout, stderr = self._client.exec_command(command)
        status = stdout.channel.recv_exit_status()
        return status, stdout, stderr


def load_config(project_id: str, project_token: str):
    if not project_id:
        logging.error(
            "project-id missing, either set METAL_PROJECT_ID environment variable, either use --project-id"
        )
        sys.exit(1)

    if not project_token:
        logging.error(
            "project-token missing, either set METAL_AUTH_TOKEN environment variable, either use --project-token"
        )
        sys.exit(1)

    with open("config.yaml") as f:
        config = yaml.safe_load(f)

    config["manager"] = packet.Manager(auth_token=project_token)
    config["project-id"] = project_id
    config["project-token"] = project_token

    return config


def _get_packet_device(name, config):
    if "devices" not in config:
        params = {"per_page": 50}

        config["devices"] = config["manager"].list_devices(
            project_id=config["project-id"], params=params
        )

    our_devices = [d for d in config["devices"] if d.hostname == name]

    if not len(our_devices):
        logging.error(f"device '{name}' doesn't exist")
        sys.exit(1)
    elif len(our_devices) > 1:
        logging.error(f"more than one device with '{name}' exist, can not continue")
        sys.exit(1)

    return our_devices[0]


def _get_ip(name, config, version, public):
    device = _get_packet_device(name, config)

    return [
        ip
        for ip in device.ip_addresses
        if ip["address_family"] == version and ip["public"] == public
    ][0]["address"]


def public_ipv4(name, config):
    return _get_ip(name, config, 4, public=True)


def public_ipv6(name, config):
    return _get_ip(name, config, 6, public=True)


def private_ipv4(name, config):
    return _get_ip(name, config, 4, public=False)


def layer2_ipv4(name, config):
    device = _get_packet_device(name, config)

    ip_mask = device["customdata"]["vlans"][0]["ip"]
    return ip_mask[: ip_mask.index("/")]


def layer2_ipv6(name, config):
    device = _get_packet_device(name, config)

    ip_mask = device["customdata"]["vlans"][0]["ipv6"]
    return ip_mask[: ip_mask.index("/")]


def get_plan(name, config):
    return _get_packet_device(name, config)["plan"]["name"]


def setup_server(name, config, ssh_con):
    public_ip = public_ipv4(name, config)
    private_ip = layer2_ipv4(name, config)
    _public_ipv6 = public_ipv6(name, config)
    _private_ipv6 = layer2_ipv6(name, config)
    plan = get_plan(name, config)

    # upload the config file
    ssh_con.bulk_upload(["k3s-config.yaml"], "/home/core")

    ssh_con.execute_commands(
        [
            # replace the placeholders values
            f"sed -i 's/SERVER_PRIVATE_IP/{private_ip}/' /home/core/k3s-config.yaml",
            f"sed -i 's/SERVER_PUBLIC_IP/{public_ip}/' /home/core/k3s-config.yaml",
            f"sed -i 's/SERVER_PRIVATE_IPV6/{_private_ipv6}/' /home/core/k3s-config.yaml",
            f"sed -i 's/SERVER_PUBLIC_IPV6/{_public_ipv6}/' /home/core/k3s-config.yaml",
            f"sed -i 's/NODE_PLAN/{plan}/' /home/core/k3s-config.yaml",
            # setup /etc/hosts
            f"sudo sed -i 's/\(127.0.0.1.*localhost\).*{name}/\\1\\n{private_ip}\\t{name}/' /etc/hosts",
            f"sudo mkdir -p /etc/rancher/k3s",
            f"sudo cp /home/core/k3s-config.yaml /etc/rancher/k3s/config.yaml",
        ]
    )


def _cluster_init(name, config):
    public_ip = public_ipv4(name, config)
    private_ip = layer2_ipv4(name, config)

    logging.info(f"connecting to {name}")
    with RemoteClient(user="core", host=public_ip) as ssh_con:
        setup_server(name, config, ssh_con)

        # install k3s
        command = f"""curl -sfL https://get.k3s.io | \
                INSTALL_K3S_CHANNEL={config['channel']} sh -s - server --cluster-init"""

        ssh_con.execute_commands([command, "sudo systemctl reboot"])


def _server_join(name, server_node, config):
    server_node_public_ip = public_ipv4(server_node, config)
    server_node_private_ip = layer2_ipv4(server_node, config)
    public_ip = public_ipv4(name, config)

    logging.info(f"connecting to {server_node} to fetch credentials")
    with RemoteClient(user="core", host=server_node_public_ip) as ssh_con:
        command = "sudo cat /var/lib/rancher/k3s/server/token"
        status, stdout, stderr = ssh_con.execute_command(command)
        token = stdout.read().rstrip().decode()

    logging.info(f"connecting to {name}")
    with RemoteClient(user="core", host=public_ip) as ssh_con:
        setup_server(name, config, ssh_con)

        # install k3s
        command = f"""curl -sfL https://get.k3s.io | \
                K3S_TOKEN={token} INSTALL_K3S_CHANNEL={config['channel']} sh -s - server \
                --server https://{server_node_private_ip}:6443"""

        ssh_con.execute_commands([command, "sudo systemctl reboot"])


def _join(name, server_node, config):
    server_node_public_ip = public_ipv4(server_node, config)
    server_node_private_ip = layer2_ipv4(server_node, config)
    public_ip = public_ipv4(name, config)
    _public_ipv6 = public_ipv6(name, config)
    private_ip = layer2_ipv4(name, config)
    _private_ipv6 = layer2_ipv6(name, config)
    plan = get_plan(name, config)

    logging.info(f"connecting to {server_node} to get a token")
    with RemoteClient(user="core", host=server_node_public_ip) as ssh_con:
        status, stdout, stderr = ssh_con.execute_command("sudo k3s token create")
        token = stdout.read().strip().decode()

    logging.info(f"connecting to {name}")
    with RemoteClient(user="core", host=public_ip) as ssh_con:
        status, stdout, stderr = ssh_con.execute_command(
            "ip -j link show type vlan | jq -r '.[].ifname' | grep -v bond0.0"
        )
        vlan = stdout.read().strip().decode()

        # install k3s
        command = f"""curl -sfL https://get.k3s.io | \
                K3S_TOKEN={token} INSTALL_K3S_CHANNEL={config['channel']} sh -s - agent \
                --node-label fdo.plan={plan} \
                --node-label svccontroller.k3s.cattle.io/enablelb=true \
                --node-label svccontroller.k3s.cattle.io/lbpool={plan} \
                --node-ip {private_ip},{_private_ipv6} \
                --node-external-ip {public_ip},{_public_ipv6} \
                --flannel-iface {vlan} \
                --server https://{server_node_private_ip}:6443"""

        ssh_con.execute_commands([command, "sudo systemctl reboot"])


def get_kilo_endpoint(ssh_con):
    status, stdout, stderr = ssh_con.execute_command(
        "sudo kubectl get node -o jsonpath='{.items[?(@.metadata.annotations.kilo\.squat\.ai/wireguard-ip!=\"\")].metadata.name}'"
    )
    kilo_endpoint = stdout.read().strip().decode()

    status, stdout, stderr = ssh_con.execute_command(
        f"sudo kubectl get node {kilo_endpoint} -o jsonpath='{{.metadata.annotations.kilo\.squat\.ai/wireguard-ip}}'"
    )
    kilo_cidr = stdout.read().strip().decode()

    return kilo_endpoint, kilo_cidr


def kubectl_apply_str(ssh_con, data):
    with tempfile.NamedTemporaryFile(delete=False) as fp:
        fp.write(bytes(data, "utf8"))

    tmpfile = fp.name
    ssh_con.bulk_upload([tmpfile], tmpfile)
    os.unlink(tmpfile)
    ssh_con.execute_commands([f"sudo kubectl apply -f {tmpfile}", f"rm {tmpfile}"])


def deploy_kilo(ssh_con):
    # deploy kilo on the server
    ssh_con.bulk_upload(["config.yaml", "kilo_kustomization.yaml"], "/home/core")

    ssh_con.execute_commands(
        [
            "mkdir -p /home/core/kilo",
            "mv /home/core/kilo_kustomization.yaml /home/core/kilo/kustomization.yaml",
            "kustomize build /home/core/kilo/ | gomplate --datasource config=/home/core/config.yaml > kilo_manifest.yaml",
            "sudo kubectl apply -f kilo_manifest.yaml",
        ]
    )


def _add_peer(name, server_name, config):
    public_ip = public_ipv4(server_name, config)

    # generate a public/private wg key pair
    with subprocess.Popen(["wg", "genkey"], stdout=subprocess.PIPE) as proc:
        private_key = proc.stdout.read().rstrip()

    with subprocess.Popen(
        ["wg", "pubkey"], stdout=subprocess.PIPE, stdin=subprocess.PIPE
    ) as proc:
        public_key, err = proc.communicate(input=private_key)
        public_key = public_key.rstrip()

    logging.info(f"connecting to {server_name}")
    with RemoteClient(
        user="core",
        host=public_ip,
    ) as ssh_con:
        # list all registered peers to get the allocated IPs
        status, stdout, stderr = ssh_con.execute_command(
            f"sudo kubectl get -A peers.kilo.squat.ai -o json"
        )
        try:
            peers = json.load(stdout)
        except json.JSONDecodeError:
            # kilo not installed
            deploy_kilo(ssh_con)
            peers = {"items": []}

        cur_ips = []
        for item in peers["items"]:
            if item["metadata"]["name"] == name:
                logging.error(f"peer {name} is already used, aborting")
                sys.exit(1)
            cur_ips.extend(item["spec"]["allowedIPs"])

        kilo_endpoint, kilo_cidr = get_kilo_endpoint(ssh_con)
        kilo_public_ip = public_ipv4(kilo_endpoint, config)
        kilo_private_ip = layer2_ipv4(kilo_endpoint, config)
        kilo_address = kilo_cidr.split("/")[0]

        cur_ips.append(ipaddress.IPv4Address(kilo_address))

        if server_name != kilo_endpoint:
            logging.info(f"connecting to {kilo_endpoint}")
            with RemoteClient(
                user="core",
                host=kilo_public_ip,
            ) as kilo_ssh_con:
                # Retrieve the public key of the server
                status, stdout, stderr = kilo_ssh_con.execute_command(
                    f"sudo wg show kilo0 public-key"
                )
                server_key = stdout.read().rstrip()
        else:
            # Retrieve the public key of the server
            status, stdout, stderr = ssh_con.execute_command(
                f"sudo wg show kilo0 public-key"
            )
            server_key = stdout.read().rstrip()

        # Find a non-assigned IP in the kilo range
        assigned_ip = None
        net = ipaddress.ip_network(kilo_cidr, strict=False)
        while assigned_ip is None:
            test_ip = random.choice(list(net.hosts()))
            if test_ip not in cur_ips:
                assigned_ip = test_ip

        # write the kilo peer config
        kubectl_apply_str(
            ssh_con,
            f"""---
apiVersion: kilo.squat.ai/v1alpha1
kind: Peer
metadata:
  name: {name}
spec:
  allowedIPs:
  - {assigned_ip}/32
  publicKey: {public_key.decode()}
  persistentKeepalive: 10""",
        )

        # retrieve the config of the cluster
        with open("k3s-config.yaml") as f:
            cluster = yaml.safe_load(f)

        # write the wireguard config file to stdout
        print(
            f"""[Interface]
Address = {assigned_ip}
PrivateKey = {private_key.decode()}
MTU = 1350

[Peer]
AllowedIPs = {cluster["cluster-cidr"]}, {cluster["service-cidr"]}, {kilo_private_ip}/32, {kilo_address}/32
Endpoint = {kilo_public_ip}:51821
PersistentKeepalive = 10
PublicKey = {server_key.decode()}"""
        )


def _make_kubeconfig(user, namespace, server_name, config):
    public_ip = public_ipv4(server_name, config)

    if namespace is not None:
        admin_name = f"admin-{namespace}-{user}"
    else:
        admin_name = f"admin-{user}"

    token_name = f"{admin_name}-token-{uuid.uuid4().hex[:6]}"

    logging.info(f"connecting to {server_name}")
    with RemoteClient(
        user="core",
        host=public_ip,
    ) as ssh_con:
        commands = []
        if namespace is None:
            namespace = "kube-system"
            commands.extend(
                [
                    f"sudo kubectl -n kube-system create serviceaccount {admin_name}",
                    f"sudo kubectl create clusterrolebinding add-on-cluster-admin-{admin_name} \
                        --clusterrole=cluster-admin \
                        --serviceaccount=kube-system:{admin_name}",
                ]
            )
            ssh_con.execute_commands(commands)
        else:
            kubectl_apply_str(
                ssh_con,
                f"""---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {admin_name}
  namespace: {namespace}
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {namespace}-admin
  namespace: {namespace}
rules:
- apiGroups: ["", "extensions", "apps"]
  resources: ["*"]
  verbs: ["*"]
- apiGroups: ["batch"]
  resources:
  - jobs
  - cronjobs
  verbs: ["*"]

---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {namespace}-{admin_name}-view
  namespace: {namespace}
subjects:
- kind: ServiceAccount
  name: {admin_name}
  namespace: {namespace}
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: {namespace}-admin
""",
            )

        # Since k8s 1.22, the token is not created automatically
        kubectl_apply_str(
            ssh_con,
            f"""---
apiVersion: v1
kind: Secret
metadata:
  name: {token_name}
  namespace: {namespace}
  annotations:
    kubernetes.io/service-account.name: "{admin_name}"
type: kubernetes.io/service-account-token
""",
        )

        status, stdout, stderr = ssh_con.execute_command(
            f"sudo kubectl -n {namespace} get secret {token_name} -o jsonpath='{{.data.token}}'"
        )
        encoded_token = stdout.read().strip()
        token = base64.decodebytes(encoded_token).decode("utf8")

        status, stdout, stderr = ssh_con.execute_command(
            f"sudo grep certificate-authority-data /etc/rancher/k3s/k3s.yaml | cut -d ':' -f 2 | xargs"
        )
        cluster_cert = stdout.read().strip()

        kilo_endpoint, kilo_cidr = get_kilo_endpoint(ssh_con)
        kilo_private_ip = layer2_ipv4(kilo_endpoint, config)

        k8s_cert_fp = tempfile.NamedTemporaryFile(delete=False)
        k8s_cert_fp.write(base64.decodebytes(cluster_cert))
        k8s_cert_fp.close()

        kube_config_fp = tempfile.NamedTemporaryFile(delete=False)
        kube_config_fp.close()

        def run_with_kubectx(cmd, stdout=subprocess.DEVNULL):
            subprocess.run(
                shlex.split(cmd),
                env={"KUBECONFIG": kube_config_fp.name, "PATH": os.getenv("PATH")},
                stdout=stdout,
            ).check_returncode()

        try:
            run_with_kubectx(
                f"kubectl config set-credentials {admin_name} --token={token}"
            )
            run_with_kubectx(
                f"""kubectl config set-cluster {server_name} \
                    --server=https://{kilo_private_ip}:6443 \
                    --certificate-authority={k8s_cert_fp.name} \
                    --embed-certs=true"""
            )
            run_with_kubectx(
                f"""kubectl config set-context \
                    admin@{server_name} \
                    --cluster {server_name} \
                    --user {admin_name}"""
            )
            run_with_kubectx(f"kubectl config use-context admin@{server_name}")

            logging.info("the following is your new kubeconfig file")
            logging.info("---")

            run_with_kubectx(f"kubectl config view --minify=true --raw=true", stdout=None)
        finally:
            os.unlink(k8s_cert_fp.name)
            os.unlink(kube_config_fp.name)


@fdo_infra.command(name="cluster-init")
@click.argument("name")
@click.pass_context
def cluster_init(ctx, name: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _cluster_init(name, config)


@fdo_infra.command(name="server-join")
@click.argument("name")
@click.argument("server-node")
@click.pass_context
def server_join(ctx, name: str, server_node: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _server_join(name, server_node, config)


@fdo_infra.command(name="join")
@click.argument("name")
@click.argument("server-node")
@click.pass_context
def join(ctx, name: str, server_node: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _join(name, server_node, config)


@fdo_infra.command(name="add-peer")
@click.option(
    "--server",
    help="one of the control plane server name",
    default="fdo-k3s-server-1",
)
@click.argument("name")
@click.pass_context
def add_peer(ctx, name: str, server: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _add_peer(name, server, config)


@fdo_infra.command(name="make-kubeconfig")
@click.option(
    "--server",
    help="one of the control plane server name",
    default="fdo-k3s-server-1",
)
@click.option(
    "--namespace",
    help="if set, the user will only have access to that namespace",
    default=None,
)
@click.argument("user")
@click.pass_context
def make_kubeconfig(ctx, user: str, server: str, namespace):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _make_kubeconfig(user, namespace, server, config)


def main(*args, **kwargs):
    fdo_infra(obj={}, *args, **kwargs)


if __name__ == "__main__":
    main()
