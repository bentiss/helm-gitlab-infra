#!/bin/bash

set -xe

# unpack all compressed filed retrieved in the butane file
tar xvf /root/kustomize_linux_amd64.tar.gz -C /usr/local/bin kustomize
tar xvf /root/helm_linux_amd64.tar.gz -C /usr/local/bin --strip-components 1 linux-amd64/helm
HOME=/root helm plugin install https://github.com/databus23/helm-diff --version master
tar xvf /root/helmfile_linux_amd64.tar.gz -C /usr/local/bin helmfile

# also clone our gitlab k3s deployment repo
cd /root
git clone https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra.git

# install subctl for cross-cluster communication
curl -Ls https://get.submariner.io | DESTDIR=/usr/local/bin/ bash
