#!/bin/bash

NAME=admin-$1

if [ "$NAME" == "admin-" ]
then
  echo "missing account name." 1>&2
  echo "usage: $0 <account_name>" 1>&2
  exit 1
fi

TOKENNAME=$(kubectl -n kube-system get serviceaccount/$NAME -o jsonpath='{.secrets[0].name}')
kubectl -n kube-system delete secret $TOKENNAME
