#!/bin/sh

set -e
set -u
set -o pipefail

# you must set:
#  - $METAL_PROJECT_ID to the fd.o CI Packet project
#  - $METAL_DEVICE_NAME to the desired device name (e.g. fdo-k3s-5) (fallbacks on PACKET_DEVICE_NAME)

METAL_DEVICE_NAME=${METAL_DEVICE_NAME:-$PACKET_DEVICE_NAME}

VLAN=42

SCRIPT_DIR=$(dirname "$(realpath "${BASH_SOURCE[0]}")")

# strip out the last '-' and everything else after
METAL_DEVICE_NAME_PREFIX=${METAL_DEVICE_NAME%-*}

# and extract the last digits of the name as the "id"
METAL_DEVICE_ID=${METAL_DEVICE_NAME#$METAL_DEVICE_NAME_PREFIX-}

case "${METAL_DEVICE_NAME_PREFIX}" in
  fdo-k3s-server)
    METAL_PLAN=c3.medium.x86
    METAL_METRO="dc"
    METAL_TAGS="k3s,k3s-server"
    VLAN_IP=172.20.${VLAN}.${METAL_DEVICE_ID}
    VLAN_IPv6=fd0a:6a61:2196:84b6:${VLAN}::${METAL_DEVICE_ID}
    ;;
  fdo-k3s-large)
    METAL_PLAN=s3.xlarge.x86
    METAL_METRO="dc"
    METAL_TAGS="k3s,k3s-large"
    # to avoid a duplicate IP, we add 1 to the vlan id and set the netmask to /23
    VLAN_IP=172.20.$((${VLAN} + 1)).${METAL_DEVICE_ID}
    VLAN_IPv6=fd0a:6a61:2196:84b6:${VLAN}:1::${METAL_DEVICE_ID}
    ;;
  *)
    echo "can't get plan from name ${METAL_DEVICE_NAME}"
    exit 1
    ;;
esac

if [ "$(metal device get --project-id $METAL_PROJECT_ID -o json | jq -r '.[] | select(.hostname == "'$METAL_DEVICE_NAME'") | .hostname')" == $METAL_DEVICE_NAME ]; then
	echo "Device name $METAL_DEVICE_NAME already taken"
	exit 1
fi

CUSTOM_DATA=$(env -i \
  jq -nc --argjson vlans \
    "$(jq -n --arg vlan ${VLAN} --arg ip ${VLAN_IP}/23 --arg ipv6 ${VLAN_IPv6}/64 '[$ARGS.named]')" \
  '{vlans: $vlans, environment: $ENV}'
)

DEVICE_ID="$(metal device create --project-id $METAL_PROJECT_ID \
                                 --hostname $METAL_DEVICE_NAME \
                                 --plan $METAL_PLAN \
                                 --metro $METAL_METRO \
                                 --tags $METAL_TAGS \
                                 --operating-system custom_ipxe \
                                 --userdata-file ${SCRIPT_DIR}/../coreos-ipxe/coreos-equinix-live.ipxe \
                                 --customdata "$CUSTOM_DATA" \
                                 --output json | jq -r .id)"

echo "Device $DEVICE_ID successfully created"

BOND0=""

while [ "$BOND0" = "" ]
do
  sleep 2
  BOND0="$(metal device get -i $DEVICE_ID -o json | jq -r '.network_ports[] | select(.name == "bond0") | .id')"
done

echo "Waiting for device to be ready"

DEVICE_STATE=""

while ! [ "$DEVICE_STATE" = "active" ]; do
	sleep 10
	DEVICE_STATE="$(metal device get --id $DEVICE_ID -o json | jq -r .state)"
done

echo "Device $DEVICE_ID became active"

echo "Assigning VLAN $VLAN to bond0 $BOND0"
metal ports vlan --port-id $BOND0 --assign $VLAN

DEVICE_IP="$(metal device get --id $DEVICE_ID -o json | jq -r '.ip_addresses[] | select(.address_family == 4 and (.address | startswith("10.") | not)) | .address')"
echo "IP: $DEVICE_IP"

SOS_ADDR="${DEVICE_ID}@sos.$(metal device get --id $DEVICE_ID -o json | jq -r .facility.code).platformequinix.com"
echo "Remote console: $SOS_ADDR"
